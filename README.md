# Distributed-Leg-Control

### What is this?
- This project is a Atmel AVR Atmega328P based control of a single leg of a multi-legged robot. </br>
- The code is written to be used with a 3DOF Hexapod/Quadruped Leg (similar to the PhantomX Hexapod). </br>
- This code uses 1 standard PWM servo motors for each DOF.</br>
- A x-y-z coordinate is received using UART (in my case via UDP -> ESP8266-01 -> UART) and the inverse kinematic is calculated using the trigonometric method and converted to be used with the servo motors.</br>


---
### Tools I used

* A handy fuse [calculator](http://eleccelerator.com/fusecalc/fusecalc.php?chip=attiny85)

* A very handy object dumped AVR code anlysis and [simulation tool](http://compilers.cs.ucla.edu/avrora/tracing.html)
