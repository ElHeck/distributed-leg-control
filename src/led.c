/*
 * led.c
 *
 *  Created on: May 7, 2017
 *      Author: aron
 */

#include "led.h"
#include "misc.h"

void initLeds(){
	LED_RED_1_DDR |= (1<<LED_RED_1_PIN);
}

void ledRed1On(){
	LED_RED_1_PORT |= (1<<LED_RED_1_PIN);
}

void ledRed1Off(){
	LED_RED_1_PORT &= ~(1<<LED_RED_1_PIN);
}

void blinkLedRed1(uint8_t howOften){
	int i;
	for (i = 0; i < howOften; i++){
		ledRed1On();
		delayMs(500);
		ledRed1Off();
		delayMs(500);
	}
}
