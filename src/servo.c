/*
 * servo.c
 *
 *  Created on: Apr 28, 2017
 *      Author: elhe
 */

#include "definitions.h"
#include "servo.h"
#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include <avr/interrupt.h>

//Timer 1
#define TOP_OVERFLOW_TIMER_1 (5000-1)

//Timer 0
#define TOP_0VERFLOW_TIMER_0 (250-1)
#define OVERFLOW_TICKS_TIMER_0 250
#define PERIOD_OVERFLOW (20-1)
#define NEW_PERIOD 0

//Both timer
#define USEC_PER_TICK 4

//global variables
volatile uint8_t tibiaCurrentPeriod = 1;
volatile uint8_t millisecondOverflow = 0;


void initServoPins() {
	//set servo pins as output
	COXA_DDR |= (1 << COXA_PIN);
	FEMUR_DDR |= (1 << FEMUR_PIN);
	TIBIA_DDR |= (1 << TIBIA_PIN);
}

void initServoTimer() {
	//Timer 1 for coxa and femur
	//Activate compare match on pin OC1A, OC1B
	TCCR1A |= (1 << COM1A1) | (1 << COM1B1);
	//set mode 14 on both registers -> fast pwm
	TCCR1A |= (1 << WGM11);
	TCCR1B |= (1 << WGM13) | (1 << WGM12);
	//set prescaler to 64 -> 1/(16MHz/64) * 5000 = 20ms TOP overflow
	TCCR1B |= (1 << CS11) | (1 << CS10);
	//set top value (when to overflow)
	ICR1 = TOP_OVERFLOW_TIMER_1;

	//TIMER 0 for the Tibia
	//fast pwm mode 7
	TCCR0A |= (1<<WGM01) | (1<<WGM00);
	TCCR0B |= (1<<WGM02);
	//set prescaler to 64 -> overflow every 1 ms at TOP = 250
	TCCR0B |= (1<<CS01) | (1<<CS00);
	//set top value
	OCR0A = TOP_0VERFLOW_TIMER_0;
	//enable overflow interrupt at TOP and OCR0B match interrupt
	TIMSK0 |= (1<<TOIE0) | (1<<OCIE0B);
}

void setAllJoints(uint8_t angle){
	setAngle(COXA, angle);
	setAngle(FEMUR, angle);
	setAngle(TIBIA, angle);
}

void setAngle(joint servo, uint8_t angle) {
	float usecInDegree = (FULL_TURN_USEC - ZERO_DEGREE_USEC) / 180.0;
	uint16_t angleInTicks = (ZERO_DEGREE_USEC + ((uint16_t) (usecInDegree * angle))) / USEC_PER_TICK;
	switch (servo) {
	case COXA:
		COXA_COMPARE_MATCH = angleInTicks;
		break;
	case FEMUR:
		FEMUR_COMPARE_MATCH = angleInTicks;
		break;
	case TIBIA:
		TIBIA_COMPARE_MATCH = (uint8_t)(angleInTicks % OVERFLOW_TICKS_TIMER_0);
		tibiaCurrentPeriod = (uint8_t) (angleInTicks / OVERFLOW_TICKS_TIMER_0);
		break;
	default:
		break;
	}
}

ISR(TIMER0_OVF_vect){
	if(millisecondOverflow == PERIOD_OVERFLOW){
		millisecondOverflow = NEW_PERIOD;
		TIBIA_OUTPUT_PORT |= (1<<TIBIA_PIN);
	}else{
		millisecondOverflow ++;
	}
}

ISR(TIMER0_COMPB_vect){
	if (millisecondOverflow == tibiaCurrentPeriod){
		TIBIA_OUTPUT_PORT &= ~(1<<TIBIA_PIN);
	}
}
