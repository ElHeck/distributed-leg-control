/*
 * servo.h
 *
 *  Created on: Apr 28, 2017
 *      Author: elhe
 */

#ifndef SERVO_H_
#define SERVO_H_

#include <avr/io.h>

#ifndef COXA_PIN
	#define COXA_PIN PB1 //OC1A
	#define COXA_DDR DDRB
	#define COXA_COMPARE_MATCH OCR1A
#endif

#ifndef FEMUR_PIN
	#define FEMUR_PIN PB2 //OC1B
	#define FEMUR_DDR DDRB
	#define FEMUR_COMPARE_MATCH OCR1B
#endif

#ifndef TIBIA_PIN
	#define TIBIA_PIN PD6 //OC0A
	#define TIBIA_DDR DDRD
	#define TIBIA_COMPARE_MATCH OCR0B
	#define TIBIA_OUTPUT_PORT PORTD
#endif

//These variables set the servo ranges
#define ZERO_DEGREE_USEC 500
#define FULL_TURN_USEC 2500


typedef enum{
	COXA = 1,
	FEMUR = 2,
	TIBIA = 3
} joint;

void initServoPins();

void initServoTimer();

void setAllJoints(uint8_t angle);

/*
 *The method used here can only be applied, of both timers have the same prescaler
 */
void setAngle(joint servo, uint8_t angle);

#endif /* SERVO_H_ */
