/*
 * smooth_servo.h
 *
 *  Created on: Jun 3, 2017
 *      Author: aron
 */

#ifndef SMOOTH_SERVO_H_
#define SMOOTH_SERVO_H_

#include "definitions.h"
#include <stdint.h>

typedef struct{
	uint8_t coxaAngle;
	uint8_t tibiaAngle;
	uint8_t femurAngle;
}legAngles;


void initSmoothServo();

void moveLeg(uint16_t msMovementDuration);

void makeAbsoluteAngleDifference(legAngles *difference, legAngles *newAngles, legAngles *oldAngles);

uint8_t getLargestAngle(legAngles *angles);

int easeLinear(uint8_t currentIncrement, uint8_t totalIncrements, uint8_t startingAngle, uint8_t targetAngle);

void simpleSetServoAngles(uint8_t coxa, uint8_t femur, uint8_t tibia);

void setNewAngles(uint8_t coxa, uint8_t femur, uint8_t tibia);

void setOldAngles(uint8_t coxa, uint8_t femur, uint8_t tibia);
#endif /* SMOOTH_SERVO_H_ */
