/*
 * esp_at_commands.h
 *
 *  Created on: May 7, 2017
 *      Author: aron
 */

#ifndef ESP_COMMANDS_H_
#define ESP_COMMANDS_H_

#define COMMAND_TYPE_OFFSET 0
#define PAYLOAD_OFFSET 1
#define SIMPLE_COMMAND_DELIMITER_OFFSET 2

#define DATA_PAYLOAD_OFFSET_1 0
#define DATA_PAYLOAD_OFFSET_2 2
#define DATA_PAYLOAD_OFFSET_3 4
#define DATA_PAYLOAD_OFFSET_4 6
#define DATA_PAYLOAD_OFFSET_5 8
#define DATA_PAYLOAD_OFFSET_6 10
#define DATA_PAYLOAD_OFFSET_7 12
#define DATA_PAYLOAD_OFFSET_8 14

//static ESP Commands

uint8_t delimiter = '#';
uint8_t initializeEsp = 'I';
uint8_t initializeAnswerNegativ[] = "UNCON";
uint8_t initializeAnswerPositiv[] = "CON";
uint8_t acknowledgeMessage[] = "OK";
uint8_t startSimpleCommand = 'C';
uint8_t startDataPayload = 'D';


#endif /* ESP_COMMANDS_H_ */
