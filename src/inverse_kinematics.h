/*
 * inverse_kinematics.h
 *
 *  Created on: Jun 22, 2017
 *      Author: aron
 */

#ifndef INVERSE_KINEMATICS_H_
#define INVERSE_KINEMATICS_H_

#include "definitions.h"
#include <stdint.h>

#define DEGREE_TO_RAD 0.01745f
#define RAD_TO_DEGREE 57.29577f
#define COXA_LENGTH 4.5f
#define FEMUR_LENGTH 6.0f
#define TIBIA_LENGTH 8.0f

typedef struct{
	float tibia;
	float coxa;
	float femur;
} floatRadiansAngles;

floatRadiansAngles solveLeg(int x, int y, int z);

void checkAcosRangeRestrictions(float *top, float *bottom);

#endif /* INVERSE_KINEMATICS_H_ */
