/*
 * definitions.h
 *
 *  Created on: Apr 23, 2017
 *      Author: elhe
 */

#ifndef DEFINITIONS_H_
#define DEFINITIONS_H_

#define ASSERT_DEBUG
//#define SERIAL_DEBUG

#define BYTE_SIZE 8

#define VERY_TINY_BUFFER 16
#define TINY_BUFFER 32
#define SHORT_BUFFER 64
#define MEDIUM_BUFFER 128
#define LONG_BUFFER 256

#define TRUE 1
#define FALSE 0

#define F_CPU 16000000UL

#define LEG_1
//#define LEG_2
//#define LEG_3
//#define LEG_4

#ifndef __AVR_ATmega328P__
	#define __AVR_ATmega328P__
#endif


typedef enum{
	NOT_DONE = 1,
	DONE = 0
}verboseLoopBreak;

typedef enum{
	FAILED = 0,
	SUCCESS = 1
} returnStatus;

#endif /* DEFINITIONS_H_ */
