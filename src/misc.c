/*
 * misc.c
 *
 *  Created on: Jun 3, 2017
 *      Author: aron
 */


#include "misc.h"
#include <util/delay.h>

void delayMs(uint16_t count){
	while (count--){
		_delay_ms(1);
	}
}
