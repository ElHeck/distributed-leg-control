/*
 * main.c
 *
 *  Created on: Apr 23, 2017
 *      Author: elhe
 */

//
//                  +-\/-+
//     10kOhm PC6  1|    |28 PC5 LED
//        RXD PD0  2|    |27 PC4
//        TXD PD1  3|    |26 PC3
//            PD2  4|    |25 PC2
//            PD3  5|    |24 PC1
//            PD4  6|    |23 PC0
//            VCC  7|    |22 GND
//            GND  8|    |21 AREF
//            PB6  9|    |20 AVCC
//            PB7 10|    |19 PB5
//            PD5 11|    |18 PB4
//      TIBIA PD6 12|    |17 PB3
//            PD7 13|    |16 PB2 FEMUR
//            PB0 14|    |15 PB1 COXA
//                  +----+
//(0,COXA_LENGTH+ 0.5, 0)

#include <avr/interrupt.h>
#include "definitions.h"
#include "led.h"
#include "leg_movement_wrapper.h"
#include "misc.h"
#include "esp8266.h"

int main(){
	cli();
	initLeds();
	initLegMovement();
	sei();
	blinkLedRed1(3);
	initEsp8266();
	blinkLedRed1(3);


	for(;;){
		receivedInstruction currentInstruction;
		uint16_t x,y,z;
		currentInstruction = waitForAnyCommand();
		switch(currentInstruction.type){
		case SIMPLE_COMMAND:
			break;
		case DATA_PAYLOAD:
			x = currentInstruction.payload.x;
			y = currentInstruction.payload.y;
			z = currentInstruction.payload.z;
			calculateAndMoveLeg(x, y, z);
			break;
		case NO_COMMAND:
			break;
		}
	}
	return 0;
}
