/*
 * usart_as_uart.c
 *
 *  Created on: May 6, 2017
 *      Author: aron
 */


#include "usart_as_uart.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#define BAUD 19200
#ifndef F_CPU
#define F_CPU 16000000UL
#endif
#include <util/setbaud.h>

//#define BAUD_RATE 115200
//#define UBRR_VALUE ((F_CPU / (BAUD_RATE * 16UL)) - 1)

//private variables
volatile uint8_t ringBuffer[RING_BUFFER_LENGTH] = {0};
volatile uint8_t rxIndex = 0;
volatile uint8_t txIndex = 0;

void initUsart(){
	//set prescaler to fit the baudrate ~ 0.2-0.5% error margin
	UBRR0H = UBRRH_VALUE;
	UBRR0L = UBRRL_VALUE;
	//set USART Mode 8 bit data, 1 stop bit Asynchronous normal mode
	UCSR0C |= (1<<UCSZ01) | (1<<UCSZ00);
	//Enable RX and Tx
	UCSR0B |= (1<<RXEN0) | (1<<TXEN0);
	//enable rx complete interrupt
	UCSR0B |= (1<<RXCIE0);
}

void sendBytes(uint8_t *buffer, uint8_t length){
	int i;
	for (i = 0; i < length; i++){
		while(!(UCSR0A & (1<<UDRE0))){};
		UDR0 = *buffer;
		buffer++;
	}
}

uint8_t readSubsequentByteFromBuffer(){
	uint8_t byteFromBuffer = ringBuffer[txIndex];
	if(txIndex == RING_BUFFER_LENGTH -1){
		txIndex = 0;
	}else{
		txIndex++;
	}
	return byteFromBuffer;
}

uint8_t readByteFromBuffer(uint8_t position){
	uint8_t currentPosition;
	if(txIndex + position > RING_BUFFER_LENGTH-1){
		currentPosition = txIndex + position - RING_BUFFER_LENGTH;
	}else {
		currentPosition = txIndex + position;
	}
	return ringBuffer[currentPosition];
}

uint8_t calculateRxTxDifference(){
	return ((rxIndex - txIndex >= 0) ? (rxIndex - txIndex) : (rxIndex + RING_BUFFER_LENGTH - txIndex));
}

ISR(USART_RX_vect){
	uint8_t value = UDR0;
	ringBuffer[rxIndex] = value;
	if(rxIndex == RING_BUFFER_LENGTH -1){
		rxIndex = 0;
	}else{
		rxIndex++;
	}
}
