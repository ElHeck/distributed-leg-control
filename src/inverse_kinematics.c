/*
 * inverse_kinematics.c
 *
 *  Created on: Jun 22, 2017
 *      Author: aron
 */

#include "inverse_kinematics.h"

#include <math.h>
#include <stdlib.h>


floatRadiansAngles solveLeg(int x, int y, int z){
	floatRadiansAngles ikRadiansAngles;
	ikRadiansAngles.coxa = atan2(x, y);
	float strideLength = sqrt((x*x) + (y*y));
	float endEffektorToCoxa =  sqrt((z * z) + ((strideLength- COXA_LENGTH) * (strideLength- COXA_LENGTH)));
	float eToCoSquared = endEffektorToCoxa * endEffektorToCoxa;
	float alpha1 = atan2(strideLength-COXA_LENGTH, z);
	float alpha2Top = eToCoSquared + (FEMUR_LENGTH * FEMUR_LENGTH) - (TIBIA_LENGTH * TIBIA_LENGTH);
	float alpha2Bottom = 2 * endEffektorToCoxa * FEMUR_LENGTH;
	checkAcosRangeRestrictions(&alpha2Top, &alpha2Bottom);
	float alpha2 = acos( alpha2Top/ alpha2Bottom);
	ikRadiansAngles.femur =alpha1 + alpha2;
	float tibiaTop = -eToCoSquared + (FEMUR_LENGTH * FEMUR_LENGTH) + (TIBIA_LENGTH * TIBIA_LENGTH);
	float tibiaBottom = 2 * FEMUR_LENGTH * TIBIA_LENGTH;
	checkAcosRangeRestrictions(&tibiaTop, &tibiaBottom);
	ikRadiansAngles.tibia = acos( tibiaTop / tibiaBottom);
	return ikRadiansAngles;
}

void checkAcosRangeRestrictions(float *top, float *bottom){
	if(fabs(*top) > fabs(*bottom)){
		*top = (*top/ fabs(*top)) *  *bottom;
	}
}
