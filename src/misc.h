/*
 * misc.h
 *
 *  Created on: Jun 3, 2017
 *      Author: aron
 */

#ifndef MISC_H_
#define MISC_H_

#include <stdint.h>


//needs to be implemented that way, because _delay_ms can only take a const at comnpiletime
void delayMs(uint16_t count);

#endif /* MISC_H_ */
