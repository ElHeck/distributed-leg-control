/*
 * smooth_servo.c
 *
 *  Created on: Jun 3, 2017
 *      Author: aron
 */


#include "smooth_servo.h"
#include "servo.h"
#include "misc.h"
#include <stdlib.h>
#include <stdio.h>


legAngles angles, lastAngles;

void initSmoothServo(){
	initServoPins();
	initServoTimer();

}

void moveLeg(uint16_t msMovementDuration){
	legAngles absoluteAngleDifference;
	uint8_t i, largestAngle, increments, timeout;
	int tempCoxaIncrement, tempFemurIncrement, tempTibiaIncrement;
	makeAbsoluteAngleDifference(&absoluteAngleDifference, &angles, &lastAngles);
	largestAngle = getLargestAngle(&absoluteAngleDifference);
	increments = largestAngle;
	timeout = (uint8_t) (msMovementDuration / largestAngle);
	for (i = 0; i < increments; i++){
		tempCoxaIncrement = easeLinear(i, increments, lastAngles.coxaAngle, angles.coxaAngle);
		tempFemurIncrement = easeLinear(i, increments, lastAngles.femurAngle, angles.femurAngle);
		tempTibiaIncrement = easeLinear(i, increments, lastAngles.tibiaAngle, angles.tibiaAngle);
		simpleSetServoAngles(lastAngles.coxaAngle + tempCoxaIncrement, lastAngles.femurAngle + tempFemurIncrement, lastAngles.tibiaAngle + tempTibiaIncrement);
		delayMs(timeout);
	}
}

void makeAbsoluteAngleDifference(legAngles *difference, legAngles *newAngles, legAngles *oldAngles){
	difference->coxaAngle = (uint8_t) abs((int)(newAngles->coxaAngle - oldAngles->coxaAngle));
	difference->femurAngle = (uint8_t) abs((int)(newAngles->femurAngle - oldAngles->femurAngle));
	difference->tibiaAngle = (uint8_t) abs((int)(newAngles->tibiaAngle - oldAngles->tibiaAngle));
}

uint8_t getLargestAngle(legAngles *angles){
	uint8_t largestAngle = angles->coxaAngle;
	if(largestAngle < angles->femurAngle){
		largestAngle = angles->femurAngle;
	}
	if(largestAngle < angles->tibiaAngle){
		largestAngle = angles->tibiaAngle;
	}
	return largestAngle;
}

int easeLinear(uint8_t currentIncrement, uint8_t totalIncrements, uint8_t startingAngle, uint8_t targetAngle){
	float dt = (float) currentIncrement/ (float)totalIncrements;
	int difference = targetAngle - startingAngle;
	return (int)(difference * dt);
}

void simpleSetServoAngles(uint8_t coxa, uint8_t femur, uint8_t tibia){
	setAngle(COXA, coxa);
	setAngle(TIBIA, tibia);
	setAngle(FEMUR, femur);
}

void setNewAngles(uint8_t coxa, uint8_t femur, uint8_t tibia){
	angles.coxaAngle = coxa;
	angles.femurAngle = femur;
	angles.tibiaAngle = tibia;
}


void setOldAngles(uint8_t coxa, uint8_t femur, uint8_t tibia){
	lastAngles.coxaAngle = coxa;
	lastAngles.femurAngle = femur;
	lastAngles.tibiaAngle = tibia;
}
