/*
 * leg_monement_wrapper.c
 *
 *  Created on: Jun 24, 2017
 *      Author: aron
 */
#include "leg_movement_wrapper.h"
#include "smooth_servo.h"
#include <stdio.h>
#include "usart_as_uart.h"

#define PI 3.14159f
#define HALF_PI 1.570795f

uint16_t movementDurationInMs = 250;

void initLegMovement(){
	uint8_t coxa = 90, femur = 0, tibia = 90;
	initSmoothServo();
	simpleSetServoAngles(coxa, femur, tibia);
	setOldAngles(coxa, femur, tibia);
}

void setAllAngles(uint8_t coxa, uint8_t femur, uint8_t tibia){
	simpleSetServoAngles(coxa, femur, tibia);
}

void calculateAndMoveLeg(int x, int y, int z){
	int16_t coxa, femur, tibia;
	#ifdef SERIAL_DEBUG
		char msg[35];
	#endif
	floatRadiansAngles calculatedAngles;
	calculatedAngles= solveLeg(x,y,z);
	#ifdef SERIAL_DEBUG
		sprintf(msg,"B x=%d y=%d z=%d c=%d f=%d t=%d\n", x,y,z, (uint8_t)(calculatedAngles.coxa * RAD_TO_DEGREE), (uint8_t)(calculatedAngles.femur * RAD_TO_DEGREE), (uint8_t)(calculatedAngles.tibia * RAD_TO_DEGREE));
		sendBytes(msg, 35);
	#endif
	adjustAnglesToPhysicalSystem(&calculatedAngles);
	coxa = (uint8_t)(calculatedAngles.coxa * RAD_TO_DEGREE);
	femur = (uint8_t)(calculatedAngles.femur * RAD_TO_DEGREE);
	tibia = (uint8_t)(calculatedAngles.tibia * RAD_TO_DEGREE);
	#ifdef SERIAL_DEBUG
		sprintf(msg,"A x=%d y=%d z=%d c=%d f=%d t=%d\n", x,y,z, coxa, femur, tibia);
		sendBytes(msg, 35);
	#endif
	setNewAngles(coxa, femur, tibia);
	moveLeg(movementDurationInMs);
	setOldAngles(coxa, femur, tibia);
}

void adjustCoordinatesToPhysicalSystem(int *x, int *y, int *z){
	#if defined(LEG_4) || defined(LEG_2) || defined(LEG_1) || defined(LEG_3)
		*y +=4;
	#endif
}

void adjustAnglesToPhysicalSystem(floatRadiansAngles *angles){
	#if defined(LEG_4) || defined(LEG_2)
		adjustLegsFirstRow(angles);
	#elif  defined(LEG_1) || defined(LEG_3)
		adjustLegsSecondRow(angles);
	#endif
	checkRangeRestriction(angles);
}

void adjustLegsFirstRow(floatRadiansAngles *angles){
	angles->tibia = PI - angles->tibia;
	angles->coxa = HALF_PI - angles->coxa;
}

void adjustLegsSecondRow(floatRadiansAngles *angles){
	angles->femur = PI - angles->femur;
	angles->coxa = HALF_PI - angles->coxa;
}

void checkRangeRestriction(floatRadiansAngles *angles){
	if (angles->femur < 0){
		angles->femur = 0;
	}else if (angles->femur >PI){
		angles->femur = PI;
	}
	if (angles->tibia < 0){
		angles->tibia = 0;
	}else if (angles->tibia >PI){
		angles->tibia = PI;
	}
	if (angles->coxa < 0){
		angles->coxa = 0;
	}else if (angles->coxa >PI){
		angles->coxa = PI;
	}
}
