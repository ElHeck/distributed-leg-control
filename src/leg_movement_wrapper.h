/*
 * leg_movement_wrapper.h
 *
 *  Created on: Jun 24, 2017
 *      Author: aron
 */

#ifndef LEG_MOVEMENT_WRAPPER_H_
#define LEG_MOVEMENT_WRAPPER_H_
#include "definitions.h"
#include <stdint.h>
#include "inverse_kinematics.h"

void initLegMovement();

void setAllAngles(uint8_t coxa, uint8_t femur, uint8_t tibia);

void calculateAndMoveLeg(int x, int y, int z);

void adjustCoordinatesToPhysicalSystem(int *x, int *y, int *z);

void adjustAnglesToPhysicalSystem(floatRadiansAngles *angles);

void adjustLegsFirstRow(floatRadiansAngles *angles);

void adjustLegsSecondRow(floatRadiansAngles *angles);

void checkRangeRestriction(floatRadiansAngles *angles);

#endif /* LEG_MOVEMENT_WRAPPER_H_ */
