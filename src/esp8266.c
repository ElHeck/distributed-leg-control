/*
 * esp8266.c
 *
 *  Created on: May 6, 2017
 *      Author: aron
 */


#include "esp8266.h"

#include <stdint.h>
#include <string.h>

#include "esp_commands.h"

uint8_t currentCommandString[SHORT_BUFFER] = {0};
uint8_t currentExtractedPayload[TINY_BUFFER] = {0};


void initEsp8266(){
	initUsart();
	sendEspInitializationAndWaitForConnection();
}

void sendEspInitializationAndWaitForConnection(){
	sendSimpleInstruction(INITIALIZE);
	waitForSimpleCommand(OK);
	waitForSimpleCommand(CONNECTED);
}

void sendSimpleInstruction(simpleInstruction instruction){
	uint8_t length = sizeof(startSimpleCommand) + sizeof(delimiter);
	uint8_t instructionString[VERY_TINY_BUFFER] = {0};
	instructionString[COMMAND_TYPE_OFFSET] = startSimpleCommand;
	instructionString[SIMPLE_COMMAND_DELIMITER_OFFSET] = delimiter;
	switch(instruction){
	case INITIALIZE:
		length += sizeof(initializeEsp);
		instructionString[PAYLOAD_OFFSET] = initializeEsp;
		break;
	}
	sendBytes(instructionString, length);
}

receivedInstruction waitForAnyCommand(){
	verboseLoopBreak status = NOT_DONE;
	receivedInstruction currentInstruction;
	currentInstruction.type = NO_COMMAND;
	while(status){
		readCommand(&currentInstruction);
		if (currentInstruction.type != NO_COMMAND){
			status = DONE;
		}
	}
	return currentInstruction;
}

void waitForSimpleCommand(simpleCommand targetCommand){
	verboseLoopBreak status = NOT_DONE;
	receivedInstruction currentInstruction;
	while(status){
		readCommand(&currentInstruction);
		if (currentInstruction.command == targetCommand){
			status = DONE;
		}
	}
}

void readCommand(receivedInstruction *receivedInstruction){
	uint8_t length = getLengthOfCommand();
	uartCommandType commandType = NO_COMMAND;
	simpleCommand currentSimpleCommand = NO_SIMPLE_COMMAND;
	dataPayload currentPayload;
	initDataPayload( &currentPayload);
	if(length != 0){
		getCommandString(length);
		commandType = identifyCommandType();
		extractPayloadFromCommandString(PAYLOAD_OFFSET, length - PAYLOAD_OFFSET);
		switch(commandType){
		case SIMPLE_COMMAND:
			currentSimpleCommand = identifySimpleCommand(length - PAYLOAD_OFFSET);
			break;
		case DATA_PAYLOAD:
			currentPayload = constructDataPayload();
			break;
		case NO_COMMAND:
			break;
		}
	}
	receivedInstruction->type = commandType;
	receivedInstruction->command = currentSimpleCommand;
	receivedInstruction->payload = currentPayload;
}

void initDataPayload(dataPayload *payload){
	payload->x = 0;
	payload->y = 0;
	payload->z = 0;
	payload->misc1 = 0;
	payload->misc2 = 0;
	payload->misc3 = 0;
	payload->misc4 = 0;
	payload->misc5 = 0;
}

uint8_t getLengthOfCommand(){
	uint8_t length = 0;
	uint8_t currentBufferLength = calculateRxTxDifference();
	uint8_t i, currentByte;
	if (currentBufferLength != 0){
		for(i = 0; i < currentBufferLength; i++){
			currentByte = readByteFromBuffer(i);
			if(currentByte == delimiter){
				length = i;
				break;
			}
		}
	}
	return length;
}

void getCommandString(uint8_t lengthOfCommand){
	int i;
	for(i = 0; i < lengthOfCommand; i++){
		currentCommandString[i] = readSubsequentByteFromBuffer();
	}
	readDelimiterIntoTrash();
}

void readDelimiterIntoTrash(){
	readSubsequentByteFromBuffer();
}

uartCommandType identifyCommandType(){
	uartCommandType commandType = NO_COMMAND;
	if(currentCommandString[COMMAND_TYPE_OFFSET] == startSimpleCommand){
		commandType = SIMPLE_COMMAND;
	}else if(currentCommandString[COMMAND_TYPE_OFFSET] == startDataPayload){
		commandType = DATA_PAYLOAD;
	}
	return commandType;
}

void extractPayloadFromCommandString(uint8_t offset, uint8_t length){
	int i;
	for(i = offset; i < length + offset; i++){
		currentExtractedPayload[i - offset] = currentCommandString[i];
	}
}

dataPayload constructDataPayload(){
	dataPayload payload;
	payload.x = (currentExtractedPayload[DATA_PAYLOAD_OFFSET_1] << BYTE_SIZE) | currentExtractedPayload[DATA_PAYLOAD_OFFSET_1 + 1];
	payload.y = (currentExtractedPayload[DATA_PAYLOAD_OFFSET_2] << BYTE_SIZE) | currentExtractedPayload[DATA_PAYLOAD_OFFSET_2 + 1];
	payload.z = (currentExtractedPayload[DATA_PAYLOAD_OFFSET_3] << BYTE_SIZE) | currentExtractedPayload[DATA_PAYLOAD_OFFSET_3 + 1];
	payload.misc1 = (currentExtractedPayload[DATA_PAYLOAD_OFFSET_4] << BYTE_SIZE) | currentExtractedPayload[DATA_PAYLOAD_OFFSET_4 + 1];
	payload.misc2 = (currentExtractedPayload[DATA_PAYLOAD_OFFSET_5] << BYTE_SIZE) | currentExtractedPayload[DATA_PAYLOAD_OFFSET_5 + 1];
	payload.misc3 = (currentExtractedPayload[DATA_PAYLOAD_OFFSET_6] << BYTE_SIZE) | currentExtractedPayload[DATA_PAYLOAD_OFFSET_6 + 1];
	payload.misc4 = (currentExtractedPayload[DATA_PAYLOAD_OFFSET_7] << BYTE_SIZE) | currentExtractedPayload[DATA_PAYLOAD_OFFSET_7 + 1];
	payload.misc5 = (currentExtractedPayload[DATA_PAYLOAD_OFFSET_8] << BYTE_SIZE) | currentExtractedPayload[DATA_PAYLOAD_OFFSET_8 + 1];
	return payload;
}

simpleCommand identifySimpleCommand(uint8_t length){
	simpleCommand command = NO_SIMPLE_COMMAND;
	if(memcmp(currentExtractedPayload, initializeAnswerPositiv, length-1) == 0){
		command = CONNECTED;
	}else if(memcmp(currentExtractedPayload, initializeAnswerNegativ, length-1) == 0){
		command = NOT_CONNECTED;
	}else if(memcmp(currentExtractedPayload, acknowledgeMessage, length-1) == 0){
		command = OK;
	}
	return command;
}
