/*
 * led.h
 *
 *  Created on: May 7, 2017
 *      Author: aron
 */

#ifndef LED_H_
#define LED_H_

#include <avr/io.h>
#include "definitions.h"

#ifndef LED_RED_1_PIN
	#define LED_RED_1_PIN PC5
	#define LED_RED_1_PORT PORTC
	#define LED_RED_1_DDR DDRC
#endif

void initLeds();

void ledRed1On();

void ledRed1Off();

void blinkLedRed1(uint8_t howOften);

#endif /* LED_H_ */
