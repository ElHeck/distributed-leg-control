/*
 * usart_as_uart.h
 *
 *  Created on: May 6, 2017
 *      Author: aron
 */

#ifndef USART_AS_UART_H_
#define USART_AS_UART_H_

#include "definitions.h"
#include <stdint.h>


#define RING_BUFFER_LENGTH 128
#define UART_BLOCKING_MAX_LOOP_COUNT 10000


void initUsart();

void sendBytes(uint8_t *buffer, uint8_t length);

uint8_t readSubsequentByteFromBuffer();

uint8_t readByteFromBuffer(uint8_t position);

uint8_t calculateRxTxDifference();



#endif /* USART_AS_UART_H_ */
