/*
 * esp8266.h
 *
 *  Created on: May 6, 2017
 *      Author: aron
 */

#ifndef ESP8266_H_
#define ESP8266_H_

#include "definitions.h"
#include "usart_as_uart.h"

//16Byte payload
typedef struct{
	uint16_t x;
	uint16_t y;
	uint16_t z;
	uint16_t misc1;
	uint16_t misc2;
	uint16_t misc3;
	uint16_t misc4;
	uint16_t misc5;
}dataPayload;

typedef enum{
	SIMPLE_COMMAND = 1,
	DATA_PAYLOAD = 2,
	NO_COMMAND= 3
}uartCommandType;

typedef enum{
	OK = 1,
	NOT_CONNECTED = 2,
	CONNECTED = 3,
	NO_SIMPLE_COMMAND = 4
}simpleCommand;

typedef enum{
	INITIALIZE = 1
}simpleInstruction;

typedef struct{
	uartCommandType type;
	simpleCommand command;
	dataPayload payload;
}receivedInstruction;


void initEsp8266();

void sendEspInitializationAndWaitForConnection();

void sendSimpleInstruction(simpleInstruction instruction);

receivedInstruction waitForAnyCommand();

void waitForSimpleCommand(simpleCommand targetCommand);

// returns a instruction with either the entire payload beeing 0 or the simple command
void readCommand(receivedInstruction *receivedInstruction);

void initDataPayload(dataPayload *payload);
/*
 *Looks for a delimiter in buffer and returns the length of the command in bytes
 *returns 0 if none found
 */
uint8_t getLengthOfCommand();

void getCommandString(uint8_t lengthOfCommand);

void readDelimiterIntoTrash();

uartCommandType identifyCommandType();

void extractPayloadFromCommandString(uint8_t offset, uint8_t length);

dataPayload constructDataPayload();

simpleCommand identifySimpleCommand(uint8_t length);

#endif /* ESP8266_H_ */
